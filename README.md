**启动服务器直接使用Python运行 server.py 就可以了**
```shell script
python server.py
```
下面是实际使用的代码
```python
from handler import BaseHandler


class SimpeHandler(BaseHandler):

    def handle(self, data_type, data):
        print(data)


if __name__ == '__main__':
    server = TCPServer(server_address=("127.0.0.1", 8080), handler_cls=SimpeHandler)
    server.run_server()

    while server.is_run:
        data = input()

        if data == "exit":
            server.close()
            break

        server.send("text", data)
```
**启动服务器直接使用Python运行 server.py 就可以了**
```shell script
python client.py
```
下面是实际使用的代码
```python
from handler import BaseHandler


class SimpeHandler(BaseHandler):

    def handle(self, data_type, data):
        print(data)

if __name__ == '__main__':
    client = TCPClient(server_address=("127.0.0.1", 8080), handler_cls=SimpeHandler)
    client.connect()

    while client.is_connected:
        data = input()

        if data == "exit":
            client.close()
            break

        client.send("text", data)
```