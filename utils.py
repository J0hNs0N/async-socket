import logging  # 引入logging模块
logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s - %(filename)s [line:%(lineno)d] - %(levelname)s: %(message)s')


def structure_reqeust(data_type, data) -> bytes:
    """
     构建数据包

    :param data_type:
    :param data:
    :return:
    """
    packet = "{type}&|&{len}&|&{data}".format(type=data_type, len=len(data), data=data)
    return packet.encode()

def parse_packet(data: bytes, split_text, recv_len):
    """

    :param data:
    :param split_text:
    :param recv_len:
    :return:
    """

    try:
        data_type, data_len, data = data.split(split_text.encode(), 2)
    except ValueError as e:
        return 'error', 0, b"", 0, True

    # 计算信息头数据长度
    head_len = (len(split_text) * 2) + len(data_len) + len(data_type)

    # 计算剩下的数据 ((数据长度 + 信息头长度) = 数据包总长) - 已经接收的 1024 字节,
    # 得到的结果就是该数据包剩下的数据长度
    return data_type, data_len, data, ((int(data_len) + head_len) - recv_len), False